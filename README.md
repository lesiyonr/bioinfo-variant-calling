## VARIANT CALLING PROJECT

<p> The project involves the alignment of 2 fastq files to a human reference geneome (hg19). After aligning the genomes a downstream analysis is carried out to determine variants presents in the provided samples. So, my task was to write a workflow pipeline to determine variants calls.</p>
<p>The main softwares need for this project are <br>
<ul> 
	<li>bwa</li>
	<li>samtools</li>
	<li>bcftools</li>
</ul>
</p>
<p>The instruction to down the softwares are indicated in the sectiion <i>DOWNLOADING REQUIRE SOFTWARES</i></p>

## DOWNLOADING REQUIED SOFTWARES
- installing software using brew: (Ideal for macbook.)

	- brew install bwa        **for doing genome alignment**
	- brew install samtools   **Changing .sam file to .bam file; making .fai indexes**
	- brew install bcftools   **calling variants**

## DOWNLOADING FASTA DATA USING wget
-  making data directory
	- mkdir data **directory for data**
	- cd data

## downloading data from broadinstitute: kept running out of memory
	wget ftp://gsapubftp-anonymous@ftp.broadinstitute.org/bundle/b37/human_g1k_v37.fasta.gz

## downloading the data from ucsc.edu
	wget --timestamping 'ftp://hgdownload.soe.ucsc.edu/goldenPath/hg19/bigZips/hg19.fa.gz'

# Uncompressing the file using gunzip
	gunzip *.gz

# INDEXING THE REFERENCE GENOME FOR FASTER DOWNSTREAM ALIGNMENT
- indexing done using bwa index:
- bwa index does burrow-wheeler transform index used to map the reads.
- the indexing gives this files: .amb, .ann, .bwt, .pac and .sa
	- cd ..
	- mkdir variantCallingAnalysis
	- cd variantCallingAnalysis

## 1• ALIGN THE READS TO hg19 build 37 genome

	bwa index -a bwtsw ../data/hg19.fa

## ALIGNMENT USING BWA mem
- aligning the reference genome: 
-  -t: for assigning threads to make the alignment faster; used 4 threads (max)
- The output is a sam (sequence alignment map) format
- sam: is a tab-delimited text file with information for each individual reads and its alignment to the ## genome.
	bwa mem -t 4 hg19.fa ..data/sample_pe_1.fq ../data/sample_pe_2.fq >aln_hg19.sam


## CHANGING .sam file to .bam file using SAMTOOLS view:
- bam is a binary version of sam; it reduces size and allows for easier data access
	samtools view -S -b aln_hg19.sam > aln_hg19.bam

## SORTING BAM file
	samtools sort alg_hg19.bam > alg_hg19_sorted.bam

## 2• GENERATE QUALITY STATISTICS FROM FASTQ or BAM fie USING YOUR TOOL OF CHOICE

## QENERATE THE QUALITY STATISTICS OF .bam file. Quality statistics piped to qualStats.txt file.
- the stats for the genomes is stored in qualStats.txt file.
- The stats shows a 97.7% proper alignment
	samtools flagstat aln_hg19.bam >qualStats.txt

## 3• CALL VARIANTS FOR THE REGIONS IN test.bed USING YOUR TOOL OF CHOICE

## INDEX REFERENCE SEQUENCE WITH samtools:
- produces a .fai index file of the reference genome: 
- samtools index: allows tools to list the chromosome and quickly fetch the sequence from reference sequence. 
	samtools faidx hg19.fa

## GENERATING GENETOTYPE OUTPUT USING MPILEUP: 
- MPILEUP command generates genotypes likelihoods at each genomic position with coverage the command is piped to bcftools calls which makes the actual variants calls
-  -T option for utilizing the bed file provided in the program:
- -Ou to work with uncompressed BCF output: prevents back and forth conversion of .bcf and .vcf files
## Output of the file: .bcf file
	bcftools mpileup -Ou -T ../data/roi.bed -f hg19.fa aln_hg19_sorted.bam > aln_hg19.bcf

## NOTES: while using -T option; no variants: running without using -T option produces variants:
	bcftools mpileup -Ou -f hg19.fa aln_hg19_sorted.bam > aln_hg19.bcf 
-  the issue might be due to use of ucsc.edu data.

## VARIANT CALLING USING BCFTOOLS CALL
-  -O output type (b = BCF)
-  -v output variant site only
-  -c uses the original consensus calling method
	bcftools call -O b -vc aln_hg19.bcf > aln_hg19_var.bcf

## FILTERING THE VARIANTS: 
	bcftools view aln_hg19_var.bcf | vcfutils.pl varFilter > aln_hg19_final.vcf

## counting alleles: 
	cat aln_hg19_final.vcf | grep -v "#" | wc -l

# CONCLUSION

<p> Overally, I enjoyed working on the project. I learnt a lot of things in the process of the project. I am hoping that my work will enable me to be further considered.</p> 



